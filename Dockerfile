FROM alpine:3.13.5

RUN addgroup -g 1000 -S app && \
    adduser -u 1000 -S app -G app -h /usr/local/app

USER app

WORKDIR /usr/local/app

COPY --chown=app:app service /usr/local/app/service

ENTRYPOINT ["/usr/local/app/service"]
